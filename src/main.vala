// Vala uses a syntax similar to C#. Anything that's in a namespace can be included by doing "using _NAMESPACE_";
// We include GTK here because we want to build a Hello World Window.
using Gtk;
using WebKit;

//Next we define our own namespace to hold our application.
namespace eTube {
    
    //Here we create our main application class. We derive from Granite.Application because it includes
    //many helper functions for us. It also includes a Granite window which is based off of the 
    //Elementary Human Interface Guildlines. You'll see in the future how much it helps us.
    public class eTube : Granite.Application {
        
        //Before we get into the constructor functions. We need to define any classwide variables.
        //Here we're going to define the MainWindow variable. We'll be assigning the GTK Window to this.
        public Window m_window;


        //Here we create the construct function for our class. Vala uses "construct" as a constructor
        //function. In this construct we define many variable that are needed for granite.
        //As time goes on we'll show that granite will take these variables and construct an about
        //dialog just for us.
        construct {
            // Your Program's Name
            program_name        = "eTube";

            //The Executable name for your program.
            exec_name           = "eTube";
            
            //The years your application is copyright.
            app_years           = "2012";

            //The icon your application uses.
            //This icon is usually stored within the "/usr/share/icons/elementary" path.
            //Put your icon in each of the respected resolution folders to enable it's use.
            app_icon            = "application-default-icon";

            //This defines the name of our desktop file. This file is used by launchers, docks,
            //and desktop environments to display an icon. We'll cover this later.
            app_launcher        = "granite_hello.desktop";

            //This is a unique ID to your application. A traditional way is to do
            // com/net/org (dot) your companies name (dot) your applications name
            //For here we have "Organization.Elementary.GraniteHello"
            application_id      = "org.elementary.granitehello";
            

            //These are very straight forward.
            //main_url = The URL linking to your website.
            main_url            = "http://christophertimberlake.com/#/post/39298284501/so-you-want-to-develop-for-elementary-os";

            //bug_url = The URL Linking to your bug tracker.
            bug_url             = "http://christophertimberlake.com/#/post/39298284501/so-you-want-to-develop-for-elementary-os";

            //help_url = The URL to your helpfiles.
            help_url            = "http://christophertimberlake.com/#/post/39298284501/so-you-want-to-develop-for-elementary-os";

            //translate_url = the URL to your translation documents.
            translate_url       = "http://christophertimberlake.com/#/post/39298284501/so-you-want-to-develop-for-elementary-os";

            //These are straight forward. Just like above.
          /*  about_authors       = {"Your Girl <YourGirl@gmail.com>"};
            about_documenters   = {"Your Guy <YourGuy@gmail.com>"};
            about_artists       = {"Your Girl <YourGirl@gmail.com>"};
            about_comments      = {"Your Guy <YourGuy@gmail.com>"};
            about_translators   = {"Bob The Translator <Bob@gmail.com>"};*/

            //What license type is this app under? I prefer MIT but theres 
            //also License.GPL_2_0 and License.GPL_3_0
            about_license_type  = License.GPL_3_0;
        }
        
        //This is another constructor. We can put GTK Overrides here...
        public eTube(){
            //For example if we wanted to use a dark theme.
            //Gtk.Settings.get_default ().gtk_application_prefer_dark_theme = true;
        }
        

        //This is our function to "activate" the GTK App. Here is where we define our startup functions.
        //Splash Screen? Initial Plugin Loading? Initial Window building? All of that goes here.
        public override void activate (){
                            //This established our window. It assigned a GTK Window to m_window. Then we manipulate it.
            //Contrary to C and some other languages. We always refer to objects or variables 
            //within our class with "this.OBJECT"
            this.m_window = new Window();

            //This sets our window's default size. 800 px width by 600 px height
            this.m_window.set_default_size(800, 600);
            this.m_window.set_hide_titlebar_when_maximized(true);
            //this.m_window.set_decorated(false);

            // This sets the Window as our Main Application Window.
            this.m_window.set_application(this);

            //This adds a button to our window.
            var webview = new WebView();
            //webview.open(Environment.get_current_dir ()+"/assets/index.html");
            webview.open("https://www.youtube.com/tv");

            
            var settings = new WebSettings();
	    settings.set("auto_resize_window",true);
	    settings.set("enable_page_cache",true);
	    settings.set("enable_plugins",true);
	    settings.set("enable_webgl",true);
	    settings.set("enable_developer_extras",false);
	    settings.set("enable_file_access_from_file_uris",true);
	    settings.set("enable_xss_auditor",false);
            settings.set("enable_universal_access_from_file_uris",true);
            
            webview.set_settings(settings);
            webview.full_content_zoom = true;
            webview.set_view_mode(WebViewViewMode.WINDOWED);


            //This adds the button to the main window.
	    var scrolled = new Gtk.ScrolledWindow (null, null);
  	    scrolled.add(webview);
            this.m_window.add(scrolled);
            this.m_window.set_resizable(true);
            
            this.m_window.window_state_event.connect((e) => {
		return false;
            });
            
            this.m_window.fullscreen();

	    var msg = new Gtk.MessageDialog (this.m_window, Gtk.DialogFlags.MODAL, Gtk.MessageType.WARNING, Gtk.ButtonsType.OK_CANCEL, "Do ALT+F4 to leave fullscreen. You can use Option+S to move it to a new workspace.");
			msg.response.connect ((response_id) => {
			switch (response_id) {
				case Gtk.ResponseType.OK:
					DoWindow();
					break;
				case Gtk.ResponseType.CANCEL:
					this.quit();
					break;
			}
			msg.destroy();
		});
		msg.show ();
        }
        private void DoWindow(){

            //This forces the window to be shown.
            this.m_window.show_all();
        }
     
    }
}



// Now that our application and window are defined, we have to call them through the main loop.
// We do this by starting with a main function. You could theoretically include all the code above
// in a main function. However it doesn't make for clean code and you wont be able to take part
// in all of granite's cool features.

// All main loop functions should start as an int. You can do self-checks within the app and 
// return 0 to force the app to close in the event of an issue.
public static int main(string[] args){

    //We need to tell GTK To start. Even though we've written all the code above. None of it
    //is started yet. So we need to start GTK then run our code.
    Gtk.init(ref args);

    //Lets assign our Application Class and Namespace to a variable. You always call it by
    // var VarName = new NameSpace.Class();
    //The Classname and Namespace Name do not have to be identical. They can be different.
    var e_Tube = new eTube.eTube();

    //Now remember how this is an Int? Instead of returning a number we're going to return our window.
    return e_Tube.run(args);
}
